<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Vocabulary extends Model
{
    protected $fillable = [
        'user_id',
        'name',
        'lang_from',
        'lang_to',
        'private',
        'description'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function langFrom()
    {
        return $this->belongsTo(Language::class, 'lang_from');
    }

    public function langTo()
    {
        return $this->belongsTo(Language::class, 'lang_to');
    }

    public function words()
    {
        return $this->hasMany(Word::class);
    }
}
