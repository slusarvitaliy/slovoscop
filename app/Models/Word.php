<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Word extends Model
{
    protected $fillable = [
        'user_id',
        'vocabulary_id',
        'word',
        'translate',
        'transcription',
        'description',
        'type',
    ];

    public function vocabulary()
    {
        return $this->belongsTo(Vocabulary::class);
    }
}
