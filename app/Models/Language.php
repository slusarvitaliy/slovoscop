<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    public function vacubalaries()
    {
        return $this->hasMany(Vocabulary::class);
    }
}
