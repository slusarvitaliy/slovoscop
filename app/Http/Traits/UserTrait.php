<?php

namespace App\Http\Traits;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

trait UserTrait
{
    public function currentUser($userId)
    {
        return Auth::check() && $userId == Auth::user()->id;
    }
}
