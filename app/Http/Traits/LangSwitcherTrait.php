<?php

namespace App\Http\Traits;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;

trait LangSwitcherTrait
{
    public function switchLang($language)
    {
        Session::remove('language');
        App::setLocale($language);
        Session::push('language', $language);

        return App::getLocale();
    }
}
