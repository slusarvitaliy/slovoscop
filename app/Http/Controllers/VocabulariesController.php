<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\Word;
use App\Models\Language;
use App\Models\Vocabulary;
use Illuminate\Http\Request;
use App\Http\Traits\UserTrait;
use Illuminate\Support\Facades\Auth;

class VocabulariesController extends Controller
{
    use UserTrait;
    public function createVocabulary(Request $request)
    {
        $result = Vocabulary::create([
            'user_id' => Auth::user()->id,
            'name' => $request->input('name'),
            'lang_from' => $request->input('lang_from'),
            'lang_to' => $request->input('lang_to'),
            'private' => $request->input('private'),
            'description' => $request->input('description'),
        ]);

        return back()->with('status', __('actions.Created Successfully'));
    }

    public function getAllVocabularies(Request $request)
    {
        $user = User::where('id', $request->id)->first();

        if(!$user) {
            return back();
        }

        $currentUser = $this->currentUser($request->id);

        $vocabularies = Vocabulary::where('user_id', $request->id)
            ->with('langFrom')
            ->with('langTo')
            ->orderBy('id', 'DESC')
            ->paginate(5);

        $languages = Language::all();

        $translate = [
            'add_word' => __('actions.Add Word'),
            'word' => __('vocabulary.Word'),
            'translate' => __('vocabulary.Translate'),
            'transcription' => __('vocabulary.Transcription'),
            'description' => __('vocabulary.Description'),
            'save' => __("actions.Save"),
            'close' => __("actions.Close"),
        ];

        return view('vocabulary.user_vocabulary', compact('user', 'languages', 'vocabularies', 'currentUser', 'translate'));

    }

    public function removeVocabulary(Request $request)
    {
        Vocabulary::where('id', $request->id)->where('user_id', Auth::user()->id)->delete();

        return back()->with('status', __('actions.Removed Successfully'));
    }

    public function editVocabulary(Request $request)
    {
        $vocabulary  = Vocabulary::where('id', $request->id)->where('user_id', Auth::user()->id)->first();

        if(!$vocabulary) {
            return back();
        }

        $languages = Language::all();

        return view('vocabulary.edit_vocabulary',  compact('vocabulary', 'languages'));
    }

    public function updateVocabulary(Request $request)
    {
        Vocabulary::where('user_id', Auth::user()->id)
            ->where('id', $request->input('id'))
            ->update([
                'name' => $request->input('name'),
                'lang_from' => $request->input('lang_from'),
                'lang_to' => $request->input('lang_to'),
                'private' => $request->input('private'),
                'description' => $request->input('description')
            ]);

        return back()->with('status', __('actions.Updated Successfully'));
    }

    public function getVocabulary(Request $request)
    {
        $vocabulary = Vocabulary::where('id', $request->id)->with('langFrom')->with('langTo')->with('user')->first();

        if(!$vocabulary) {
            return back();
        }

        $words = Word::where('vocabulary_id', $request->id)->orderBy('id', 'DESC')->paginate(10);

        $currentUser = $this->currentUser($vocabulary->user_id);

        return view('vocabulary.vocabulary',  compact('vocabulary', 'currentUser', 'words'));
    }


}
