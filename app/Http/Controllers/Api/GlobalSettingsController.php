<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Traits\LangSwitcherTrait;

class GlobalSettingsController extends Controller
{
    use LangSwitcherTrait;

    public function setLanguageToSession(Request $request)
    {
        $currentLang = $this->switchLang($request->input('language'));

        return response()->json(['lang' => $currentLang], 200);
    }
}
