<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use \App\Mail\BugReport;

class MailController extends Controller
{
    public function reportBug(Request $request)
    {
        $data = [
            'body' => $request->input('body'),
            'user' => Auth::user()->name,
            'user_id' => Auth::user()->id,
        ];

        Mail::to(env("MAIL_USERNAME"))->send(new BugReport($data));

        return response()->json(['status' => 'sent'], 200);
    }
}
