<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Word;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WordsController extends Controller
{
    public function addWord(Request $request)
    {
        $result = Word::create([
            'user_id' => Auth::user()->id,
            'vocabulary_id' => $request->input('vocabulary_id'),
            'word' => $request->input('word'),
            'translate' => $request->input('translate'),
            'transcription' => $request->input('transcription'),
            'description' => $request->input('description'),
            'type' => $request->input('type'),
        ]);

        return response()->json(['result' => $result, 'status' => __('actions.Added Successfully')], 200);
    }
}
