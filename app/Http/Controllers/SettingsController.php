<?php

namespace App\Http\Controllers;

use App\Http\Traits\UserTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SettingsController extends Controller
{
    use UserTrait;

    public function getSettings(Request $request)
    {
        $currentUser = $this->currentUser(Auth::user()->id);
        return view('user.settings');
    }
}
