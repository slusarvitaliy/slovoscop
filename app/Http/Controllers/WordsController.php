<?php

namespace App\Http\Controllers;

use App\Http\Traits\UserTrait;
use App\Models\Word;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;

class WordsController extends Controller
{
    use UserTrait;

    public function showWord(Request $request)
    {
        $word = Word::where('id', $request->id)->with('vocabulary')->first();

        if(!$word) {
            return back();
        }

        $currentUser = $this->currentUser($word->user_id);

        return view('vocabulary.word', compact('word', 'currentUser'));
    }

    public function addWord(Request $request)
    {
        Word::create([
            'user_id' => Auth::user()->id,
            'vocabulary_id' => $request->input('vocabulary_id'),
            'word' => $request->input('word'),
            'translate' => $request->input('translate'),
            'transcription' => $request->input('transcription'),
            'description' => $request->input('description'),
            'type' => $request->input('type'),
        ]);

        return back()->with('status', __('actions.Added Successfully'));
    }

    public function removeWord(Request $request)
    {
        $result = Word::where('id', $request->id)->where('user_id', Auth::user()->id)->delete();
        return back()->with('status', __('actions.Removed Successfully'));
    }

    public function editWord(Request $request)
    {
        $word = Word::where('id', $request->id)->where('user_id', Auth::user()->id)->with('vocabulary')->first();

        if(!$word) {
            return back();
        }

        return view('vocabulary.edit_word',  compact('word'));
    }

    public function updateWord(Request $request)
    {
        Word::where('user_id', Auth::user()->id)
            ->where('id', $request->input('id'))
            ->update([
                'word' => $request->input('word'),
                'translate' => $request->input('translate'),
                'transcription' => $request->input('transcription'),
                'description' => $request->input('description'),
                'type' => $request->input('type'),
            ]);

        return back()->with('status', __('actions.Updated Successfully'))->with('back', $request->input('back'));
    }
}
