<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;

class SetLanguage
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $this->languageSetLocale($request->language);

//        if(empty(Session::get('language')) && !empty($request->language)) {
//            App::setLocale($request->language);
//            Session::push('language', $request->language);
//        }

        return $next($request);
    }

    public function languageSetLocale($language)
    {
        if(empty($language) && !empty(Session::get('language'))){

            $language = Session::get('language')[0];
            Session::remove('language');

        }

        Session::remove('language');
        App::setLocale($language);
        Session::push('language', $language);
    }
}
