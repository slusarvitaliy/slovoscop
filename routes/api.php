<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('word')->namespace('Api')->group(function () {
    Route::post('add', 'WordsController@addWord');
});

Route::prefix('mail')->namespace('Api')->group(function () {
    Route::post('report-bug', 'MailController@reportBug');
});

Route::namespace('Api')->group(function () {
    Route::post('set-session-lang', 'GlobalSettingsController@setLanguageToSession');
});
