<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/',  '/en');

Route::prefix('{language?}')->where(['language' => '[a-zA-Z]{2}'])->group(function ()  {
    Route::get('/', function () {
        return view('welcome');
    });

    Auth::routes();

    Route::get('/home', 'HomeController@index')->name('home');


    Route::prefix('admin')->namespace('Admin')->group(function () {
        Route::get('user-list', 'UserListController@getAllUsers')->name('admin-user-list');
    });

    Route::prefix('vocabularies')->group(function () {
        Route::get('{id}', 'VocabulariesController@getAllVocabularies');
        Route::get('edit/{id}', 'VocabulariesController@editVocabulary')->middleware('auth');
    });

    Route::prefix('vocabulary')->group(function () {
        Route::get('{id}', 'VocabulariesController@getVocabulary');
    });

    Route::prefix('word')->group(function () {
        Route::get('/{id}', 'WordsController@showWord');
        Route::get('edit/{id}', 'WordsController@editWord')->middleware('auth');
    });

    Route::prefix('settings')->middleware('auth')->group(function () {
        Route::get('/', 'SettingsController@getSettings')->name('settings');
        Route::get('edit', 'SettingsController@editSettings');
    });
});

Route::prefix('vocabularies')->middleware('auth')->group(function () {
    Route::post('add', 'VocabulariesController@createVocabulary')->name('add-vocabulary');
    Route::post('update', 'VocabulariesController@updateVocabulary');
    Route::get('remove/{id}', 'VocabulariesController@removeVocabulary');
});

Route::prefix('word')->middleware('auth')->group(function () {
    Route::post('add', 'WordsController@addWord');
    Route::post('update', 'WordsController@updateWord');
    Route::get('remove/{id}', 'WordsController@removeWord');
});




