<?php

return [

    'Actions' => 'Actions',
    'Edit' => 'Edit',
    'Remove' => 'Remove',
    'More' => 'More',
    'Save' => 'Save',
    'Close' => 'Close',
    'Add Word' => 'Add Word',
    'Added Successfully' => 'Added Successful',
    'Updated Successfully' => 'Updated Successful',
    'Removed Successfully' => 'Removed Successful',
    'Created Successfully' => 'Created Successful',
    'Report a bug' => 'Report a bug',
    'Settings' => 'Settings',
    '' => '',

];
