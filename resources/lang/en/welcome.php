<?php
return [
    'welcome' => 'Slovoscop',
    'slogan' => 'Vocabulary service for learners of new languages',
    'Start using' => 'Start using',
    'Login' => 'Login',
    'Register' => 'Register',

];
