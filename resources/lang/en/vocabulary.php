<?php

return [

    'Vocabularies' => 'Vocabularies',
    'Vocabulary' => 'Vocabulary',
    'Add New Vocabulary' => 'Add New Vocabulary',
    'Vocabulary Name' => 'Vocabulary Name',
    'Select language' => 'Select language',
    'From language' => 'From language',
    'To language' => 'To language',
    'Vocabulary Description' => 'Vocabulary Description',
    'Word' => 'Word',
    'Translate' => 'Translate',
    'Transcription' => 'Transcription',
    'Description' => 'Description',

];
