<?php

return [

    'Actions' => 'Działania',
    'Edit' => 'Edytuj',
    'Remove' => 'Usunąć',
    'More' => 'Więcej',
    'Save' => 'Uratować',
    'Close' => 'Blisko',
    'Add Word' => 'Dodaj słowo',
    'Added Successfully' => 'Pomyslnie dodano',
    'Updated Successfully' => 'Pomyślnie zaktualizowany',
    'Removed Successfully' => 'Pomyślnie usunięty',
    'Created Successfully' => 'Pomyślnie utworzony',
    'Report a bug' => 'Zgłosić błąd',
    'Settings' => 'Ustawienia',
];
