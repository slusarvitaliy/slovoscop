<?php

return [

    'Vocabularies' => 'Słowniki',
    'Vocabulary' => 'Słownictwo',
    'Add New Vocabulary' => 'Nowego słownika dodaj',
    'Vocabulary Name' => 'Nazwa słownika',
    'Select language' => 'Wybierz język',
    'From language' => 'z opisów językowych',
    'To language' => 'do opisów językowych',
    'Vocabulary Description' => 'Opis słownika',
    'Word' => 'Słowo',
    'Translate' => 'Przetłumaczyć',
    'Transcription' => 'Transkrypcja',
    'Description' => 'Opis',
    '' => '',

];
