<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'   => 'Błędny login lub hasło.',
    'throttle' => 'Za dużo nieudanych prób logowania. Proszę spróbować za :seconds sekund.',
    'Login' => 'Logowania',
    'Logout' => 'Wylogowanie',
    'Register' => 'Registerie',
    'E-Mail Address' => 'Adres e-mail',
    'Password' => 'Hasło',
    'Remember Me' => 'Pamiętasz mnie',
    'Forgot Your Password?' => 'Nie pamiętasz hasła?',
    'Confirm Password' => 'Potwierdź hasło',
    'Name' => 'Imię',
    '' => '',
];
