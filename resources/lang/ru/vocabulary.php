<?php

return [

    'Vocabularies' => 'Словари',
    'Vocabulary' => 'Словарь',
    'Add New Vocabulary' => 'Добавить словарь',
    'Vocabulary Name' => 'Название Словаря',
    'Select language' => 'Выбрать Язык',
    'From language' => 'С Языка',
    'To language' => 'На Язык',
    'Vocabulary Description' => 'Описание Словаря',
    'Word' => 'Слово',
    'Translate' => 'Перевод',
    'Transcription' => 'Транскрипция',
    'Description' => 'Описание',
    '' => '',

];
