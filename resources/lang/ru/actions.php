<?php

return [

    'Actions' => 'Действия',
    'Edit' => 'Редактировать',
    'Remove' => 'Удалить',
    'More' => 'Подробнее',
    'Save' => 'Сохранить',
    'Close' => 'Закрыть',
    'Add Word' => 'Добавить Слово',
    'Added Successfully' => 'Успешно Добавлено',
    'Updated Successfully' => 'Успешно Обновлено',
    'Removed Successfully' => 'Успешно Удалено',
    'Created Successfully' => 'Успешно Созданно',
    'Report a bug' => 'Сообщить об ошибке',
    'Settings' => 'Настройки',
];
