<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ __('welcome.welcome') }}</title>
        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('css/welcome.css') }}" rel="stylesheet">
    </head>
    <body>
        <div class="flex-center position-ref full-height" id="app">
            @if (Route::has('login'))
                <div class="top-right links ">
                    <ul class="nav float-left welcome-lang-switcher">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                                {{strtoupper(app()->getLocale())}}
                            </a>
                            <div class="dropdown-menu">
                                @foreach (config('app.available_locales') as $locale)
                                    <a class="dropdown-item lang-switcher"
                                       href="\{{$locale}}"
                                       @if (app()->getLocale() == $locale) style="font-weight: bold; text-decoration: underline" @endif
                                       data-lang="{{$locale}}"
                                    >
                                        {{ strtoupper($locale) }}
                                    </a>
                                @endforeach
                            </div>
                            <lang-switcher/>
                        </li>
                    </ul>
                    @auth
                        <a href="{{ url(app()->getLocale().'/home') }}">{{__("welcome.Start using")}}</a>
                    @else
                        <a href="{{ route('login', app()->getLocale()) }}">{{__("auth.Login")}}</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register', app()->getLocale()) }}">{{__("auth.Register")}}</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    {{ __('welcome.welcome') }}
                </div>
                <h2>{{ __('welcome.slogan') }}</h2>


{{--                <div class="links">--}}
{{--                    <a href="https://laravel.com/docs">Docs</a>--}}
{{--                    <a href="https://laracasts.com">Laracasts</a>--}}
{{--                    <a href="https://laravel-news.com">News</a>--}}
{{--                    <a href="https://blog.laravel.com">Blog</a>--}}
{{--                    <a href="https://nova.laravel.com">Nova</a>--}}
{{--                    <a href="https://forge.laravel.com">Forge</a>--}}
{{--                    <a href="https://vapor.laravel.com">Vapor</a>--}}
{{--                    <a href="https://github.com/laravel/laravel">GitHub</a>--}}
{{--                </div>--}}
            </div>
        </div>
    </body>
</html>
