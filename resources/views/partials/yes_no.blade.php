<!-- Modal -->
<div class="modal fade" id="yesNoModal" tabindex="-1" aria-labelledby="yesNoModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="yesNoModalLabel">{{__("partials/yes_no_modal.Are you sure?")}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <a href="{{$link ?? ''}}" type="button" class="btn btn-danger yes-link" >{{__("partials/yes_no_modal.Yes")}}</a>
                <button type="button" class="btn btn-primary" data-dismiss="modal">{{__('partials/yes_no_modal.Close')}}</button>
            </div>
        </div>
    </div>
</div>

