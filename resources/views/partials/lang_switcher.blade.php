<li class="nav-item dropdown">
    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
        {{ strtoupper(app()->getLocale()) }}
    </a>
    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
        @foreach (config('app.available_locales') as $locale)
            <a class="dropdown-item lang-switcher"
               href="/{{$locale}}{{substr(request()->path(), 2)}}"
               @if (app()->getLocale() == $locale) style="font-weight: bold; text-decoration: underline" @endif
               data-lang="{{$locale}}"
            >
                {{ strtoupper($locale) }}
            </a>
        @endforeach
    </div>
    <lang-switcher/>
</li>
