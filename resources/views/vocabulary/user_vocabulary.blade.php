@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-8 col-sm-12">
                                {{$user->name}}
                            </div>
                            <div class="col-md-4 col-sm-12">
                                @auth()
                                    @if($currentUser)
                                        <div class="float-right">
                                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                                    data-target="#addVocabulary">
                                                <i class="fas fa-plus-circle"></i>
                                                {{__('vocabulary.Vocabulary')}}
                                            </button>
                                        </div>
                                    @endif
                                @endauth
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        @foreach( $vocabularies as $vocabulary)
                            <div class="card my-3">
                                <div class="card-header">
                                    <a href="/{{app()->getLocale()}}/vocabulary/{{$vocabulary->id}}">{{$vocabulary->name}}</a>
                                    <div class="float-right">
                                        {{strtoupper($vocabulary->langFrom->iso_639_1)}} - {{strtoupper($vocabulary->langTo->iso_639_1)}}
                                    </div>
                                </div>
                                <div class="card-body">
                                    <h5 class="card-title"> {{$vocabulary->langFrom->name}} - {{$vocabulary->langTo->name}}</h5>
                                    <p class="card-text">{{mb_substr($vocabulary->description, 1, 150)}}</p>
                                    @auth()
                                        @if($currentUser)
                                            <a href="#"
                                               class="btn btn-primary add-word-button"
                                               data-vocabulary-id="{{$vocabulary->id}}"
                                               data-toggle="modal" data-target="#addVueWord"
                                            >
                                                <i class="fas fa-plus-circle"></i>
                                                {{ __('vocabulary.Word') }}
                                            </a>
                                            <div class="float-right">
                                                <a href="/{{app()->getLocale()}}/vocabularies/edit/{{$vocabulary->id}}" class="btn btn-warning">
                                                    <i class="fas fa-pen-nib"></i>
{{--                                                    {{ __('actions.Edit') }}--}}
                                                </a>
                                                <a href="#" class="btn btn-danger remove-btn"
                                                   data-href="/vocabularies/remove/{{$vocabulary->id}}"
                                                   data-toggle="modal" data-target="#yesNoModal"
                                                >
                                                    <i class="far fa-trash-alt"></i>
{{--                                                    {{ __('actions.Remove') }}--}}
                                                </a>
                                            </div>
                                        @endif
                                    @endauth
                                </div>
                            </div>
                        @endforeach

                            {{ $vocabularies->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('vocabulary.partials.add_vocabulary_modal', ['languages' => $languages])
    @include('partials.yes_no')
    <add-word-modal
        translation="{{json_encode($translate)}}"
    />

@endsection

@section('script')

@endsection
