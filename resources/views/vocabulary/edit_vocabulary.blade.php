@extends('layouts.app')

@section('style')
@endsection

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-8 col-sm-12">
                                <a href="/{{app()->getLocale()}}/vocabularies/{{$vocabulary->user->id}}">
                                    <i class="fas fa-angle-left fa-lg"></i>
                                    {{$vocabulary->user->name}}
                                </a>
                                / {{$vocabulary->name}} ( {{strtoupper($vocabulary->langFrom->iso_639_1)}} - {{strtoupper($vocabulary->langTo->iso_639_1)}} )
                            </div>
                            <div class="col-md-4 col-sm-12">

                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                            <form method="post" action="/vocabularies/update">
                                @csrf
                                <input type="hidden" name="id" value="{{$vocabulary->id}}">
                                <div class="form-group">
                                    <label for="vocabularyName">{{__('vocabulary.Vocabulary Name')}}</label>
                                    <input type="text" class="form-control" id="vocabularyName" name="name" value="{{$vocabulary->name}}">
                                </div>
                                <div class="form-group">
                                    <label for="vocabularyFromLanguage">{{ __('vocabulary.From language') }}</label>
                                    <select class="form-control" id="vocabularyFromLanguage" name="lang_from">
                                        <option disabled>{{ __('vocabulary.Select language') }}</option>
                                        @foreach($languages as $lang)
                                            <option value="{{$lang->id}}" {{ $lang->id == $vocabulary->lang_from ? 'selected' : ''}}>
                                                {{$lang->name}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="vocabularyToLanguage">{{ __('vocabulary.To language') }}</label>
                                    <select class="form-control" id="vocabularyToLanguage" name="lang_to">
                                        <option disabled>{{ __('vocabulary.Select language') }}</option>
                                        @foreach($languages as $lang)
                                            <option value="{{$lang->id}}" {{ $lang->id == $vocabulary->lang_to ? 'selected' : ''}}>
                                                {{$lang->name}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="vocabularyDescription">{{__('vocabulary.Vocabulary Description')}}</label>
                                    <textarea class="form-control js-editor" id="vocabularyDescription" rows="15" name="description">{{$vocabulary->description }}</textarea>
                                </div>
                                <button type="submit" class="btn btn-primary">{{ __('actions.Save') }}</button>
                            </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')

@endsection

