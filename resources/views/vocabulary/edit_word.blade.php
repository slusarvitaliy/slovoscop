@extends('layouts.app')

@section('style')
@endsection

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-8 col-sm-12">

{{--                                <a href="/{{app()->getLocale()}}/vocabulary/{{$word->vocabulary_id}}">--}}
                                @if (session('back'))
                                    <a href="{{ session('back') }}">
                                @else
                                <a href="{{ URL::previous() }}">
                                @endif
                                    <i class="fas fa-angle-left fa-lg"></i>
                                    {{$word->vocabulary->name}}
                                </a>
                            </div>
                            <div class="col-md-4 col-sm-12">

                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                            <form action="/word/update" method="post">
                                @csrf
                                <input type="hidden" name="id" value="{{$word->id}}">
                                <input type="hidden" name="back" value="{{ URL::previous() }}">

                                <div class="form-group">
                                    <label for="word">{{ __('vocabulary.Word') }}</label>
                                    <input type="text"  name="word" class="form-control" id="word" required value="{{$word->word}}">
                                </div>

                                <div class="form-group">
                                    <label for="translate">{{__('vocabulary.Translate')}}</label>
                                    <input type="text"  name="translate" class="form-control" id="translate" required value="{{$word->translate}}">
                                </div>

                                <div class="form-group">
                                    <label for="transcription">{{__('vocabulary.Transcription')}}</label>
                                    <input type="text"  name="transcription" class="form-control" id="transcription" value="{{$word->transcription}}">
                                </div>

                                <div class="form-group">
                                    <label for="description">{{__('vocabulary.Description')}}</label>
                                    <textarea class="form-control" id="description" rows="3" name="description" >{{$word->description}}</textarea>
                                </div>

                                <button type="submit" class="btn btn-primary">{{__("actions.Save")}}</button>
{{--                                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__("actions.Close")}}</button>--}}
                            </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')

@endsection

