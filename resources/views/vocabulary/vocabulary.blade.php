@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-8 col-sm-12">
                                <a href="/{{app()->getLocale()}}/vocabularies/{{$vocabulary->user->id}}">
                                    <i class="fas fa-angle-left fa-lg"></i>
                                    {{$vocabulary->user->name}}
                                </a>
                                / {{$vocabulary->name}} ( {{strtoupper($vocabulary->langFrom->iso_639_1)}} - {{strtoupper($vocabulary->langTo->iso_639_1)}} )
                            </div>
                            <div class="col-md-4 col-sm-12">
                                @auth()
                                @if($currentUser)
                                    <div class="float-right">
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addWord">
                                            <i class="fas fa-plus-circle"></i>
                                            {{ __('actions.Add Word') }}
                                        </button>
                                    </div>
                                @endif
                            @endauth
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <div>
                            <p>
                                {{$vocabulary->description}}
                            </p>
                        </div>

                        <ul class="list-group">
                            @foreach($words as $item)
                                <li class="list-group-item">
                                    <div  class="row">
                                        <a href="/{{app()->getLocale()}}/word/{{$item->id}}" class="col-md-5 col-sm-12 mb-2">
                                            {{mb_substr($item->word,0, 50)}}
                                        </a>
                                        <div class="col-md-5 col-sm-12">
                                            {{mb_substr($item->translate,0, 50)}}
                                        </div>
                                        <div class="col-md-2 col-sm-12">
                                            @auth()
                                                @if($currentUser)
                                                    <div class="float-right">
                                                        <a href="/{{app()->getLocale()}}/word/edit/{{$item->id}}" class="btn btn-warning">
                                                            <i class="fas fa-pen-nib"></i>
                                                        </a>
                                                        <a href="#"
                                                           class="btn btn-danger remove-btn"
                                                           data-href="/word/remove/{{$item->id}}"
                                                           data-toggle="modal" data-target="#yesNoModal"
                                                        >
                                                            <i class="far fa-trash-alt"></i>
                                                        </a>
                                                    </div>
                                                @endif
                                            @endauth
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        </ul>

                        <div class="my-3">
                            {{ $words->links() }}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('vocabulary.partials.add_word', ['vocabulary_id' => $vocabulary->id])
    @include('partials.yes_no')
@endsection

@section('script')

@endsection

