@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-8 col-sm-12">
                                <a href="{{ URL::previous() }}"><i class="fas fa-angle-left fa-lg"></i> Back</a> /
                                <a href="/{{app()->getLocale()}}/vocabulary/{{$word->vocabulary_id}}">

                                    {{$word->vocabulary->name}}
                                </a>
                            </div>
                            <div class="col-md-4 col-sm-12">
                                @auth()
                                    @if($currentUser)
                                        <div class="float-right">
                                            <a href="/{{app()->getLocale()}}/word/edit/{{$word->id}}" type="button" class="btn btn-warning">
                                                <i class="fas fa-plus-circle"></i>
                                                {{ __('actions.Edit') }}
                                            </a>
                                        </div>
                                    @endif
                                @endauth
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <p>
                                    <span class="badge badge-primary">{{ __('vocabulary.Word') }}</span>
                                    {{$word->word}}
                                </p>
                            </div>

                            <div class="col-md-12 col-sm-12">
                                <p>
                                    <span class="badge badge-primary">{{ __('vocabulary.Translate') }}</span>
                                    {{$word->translate}}
                                </p>
                            </div>

                            @if($word->description)
                                <div class="col-md-12 col-sm-12">
                                    <p>
                                        <span class="badge badge-primary">{{ __('vocabulary.Description') }}</span>
                                        {{$word->description}}
                                    </p>
                                </div>
                            @endif
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('partials.yes_no')
@endsection

@section('script')

@endsection

