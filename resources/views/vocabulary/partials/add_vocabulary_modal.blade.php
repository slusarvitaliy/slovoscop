<div class="modal fade" id="addVocabulary" tabindex="-1" aria-labelledby="addVocabularyLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addVocabularyLabel">{{__('vocabulary.Add New Vocabulary')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form method="post" action="{{route('add-vocabulary')}}">
                    @csrf
                    <div class="form-group">
                        <label for="vocabularyName">{{__('vocabulary.Vocabulary Name')}}</label>
                        <input type="text" class="form-control" id="vocabularyName" name="name" required>
                    </div>
                    <div class="form-group">
                        <label for="vocabularyFromLanguage">{{ __('vocabulary.From language') }}</label>
                        <select class="form-control" id="vocabularyFromLanguage" name="lang_from" required>
                            <option disabled>{{ __('vocabulary.Select language') }}</option>
                            @foreach($languages as $lang)
                                <option value="{{$lang->id}}">
                                    {{$lang->name}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="vocabularyToLanguage">{{ __('vocabulary.To language') }}</label>
                        <select class="form-control" id="vocabularyToLanguage" name="lang_to" required>
                            <option disabled>{{ __('vocabulary.Select language') }}</option>
                            @foreach($languages as $lang)
                                <option value="{{$lang->id}}">{{$lang->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="vocabularyDescription">{{__('vocabulary.Vocabulary Description')}}</label>
                        <textarea class="form-control" id="vocabularyDescription" rows="3" name="description"></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary">{{ __('actions.Save') }}</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('actions.Close') }}</button>
                </form>

            </div>
        </div>
    </div>
</div>
