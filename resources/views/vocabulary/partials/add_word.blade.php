<div>
    <div class="modal fade" id="addWord" tabindex="-1" aria-labelledby="addWordLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="addWordLabel">{{ __('actions.Add Word') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="/word/add" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="word">{{ __('vocabulary.Word') }}</label>
                            <input type="text"  name="word" class="form-control" id="word" required>
                        </div>

                        <div class="form-group">
                            <label for="translate">{{__('vocabulary.Translate')}}</label>
                            <input type="text"  name="translate" class="form-control" id="translate" required>
                        </div>

                        <div class="form-group">
                            <label for="transcription">{{__('vocabulary.Transcription')}}</label>
                            <input type="text"  name="transcription" class="form-control" id="transcription">
                        </div>

                        <div class="form-group">
                            <label for="description">{{__('vocabulary.Description')}}</label>
                            <textarea class="form-control" id="description" rows="3" name="description" ></textarea>
                        </div>

                        <input type="hidden" name="vocabulary_id" value="{{$vocabulary_id}}">

                        <button type="submit" class="btn btn-primary">{{__("actions.Save")}}</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__("actions.Close")}}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

