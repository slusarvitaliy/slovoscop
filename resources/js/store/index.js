import Vue from 'vue';
import Vuex from 'vuex';
import words from "./modules/words";
import globalSettings from "./modules/globalSettings";
import mail from "./modules/mail";

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        words,
        globalSettings,
        mail,
    },
})
