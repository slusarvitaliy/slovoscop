import Vue from 'vue';
export default {
    state: {
        userWords: [],
        vocabularyWords: [],
    },

    getters: {
        getAllUserWords: ({userWords}) => userWords,
        getVocabularyWords: ({vocabularyWords}) => vocabularyWords,
    },

    actions: {
        ajaxCategoriesFromDB( {commit}, test ) {
            console.log(test);
            axios.get("api/category")
                .then(response => {
                    console.log(response.data.categories);
                    commit('setCategories', response.data.categories)
                })
                .catch(error => console.log(error));
        },

        addWord({commit}, request) {
            console.log('word.js');
            axios.post("/api/word/add", request)
                .then(response => {
                    console.log('response',response.data.status);
                    Vue.notify({
                        group: 'foo',
                        type: 'success',
                        text: response.data.status
                    })
                })
                .catch(error => console.log(error));
        }
    },

    mutations: {
        setCategories(state, data) {
            return state.categories = data;
        }
    }
}
