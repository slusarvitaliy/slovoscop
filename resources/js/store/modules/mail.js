import Vue from 'vue';
export default {
    state: {

    },

    getters: {

    },

    actions: {
        reportBug({commit}, request) {
            console.log('mail.js', request);
            axios.post("/api/mail/report-bug", request)
                .then(response => {
                    console.log('response',response.data.status);
                    Vue.notify({
                        group: 'foo',
                        type: 'success',
                        text: response.data.status
                    })
                })
                .catch(error => console.log(error));
        }
    },

    mutations: {

    }
}
