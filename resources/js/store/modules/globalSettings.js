import Vue from 'vue';
export default {
    state: {
        userWords: [],
        vocabularyWords: [],
    },

    getters: {

    },

    actions: {
        setLanguageToSession({commit}, request) {
            axios.post("/api/set-session-lang", request)
                .then(response => {
                    console.log('response',response.data);
                })
                .catch(error => console.log(error));
        }
    },

    mutations: {

    }
}
