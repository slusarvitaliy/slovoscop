class GlobalScripts {
    removeBtn() {
        if(document.querySelectorAll('.remove-btn')) {
            let removeBtn = document.querySelectorAll('.remove-btn');
            removeBtn.forEach((element) => {
                let link = element.getAttribute('data-href');
                element.addEventListener('click', () => {
                    document.querySelector('.yes-link').setAttribute('href', link);
                });
            });
        }
    }
}

document.addEventListener("DOMContentLoaded", () => {
    let global = new GlobalScripts();
    global.removeBtn();
});
